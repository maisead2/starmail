package com.eclipsekingdom.starmail.util.storage;

public interface ICopyable<T> {

    T copy();

}
