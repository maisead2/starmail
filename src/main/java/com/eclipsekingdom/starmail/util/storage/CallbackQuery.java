package com.eclipsekingdom.starmail.util.storage;

public interface CallbackQuery<T> {

    void onQueryDone(T t);

}
