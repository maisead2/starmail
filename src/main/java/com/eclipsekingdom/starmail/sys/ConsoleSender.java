package com.eclipsekingdom.starmail.sys;

import org.bukkit.Bukkit;

public class ConsoleSender {

    public static void sendMessage(String message) {
        Bukkit.getConsoleSender().sendMessage("[StarMail] " + message);
    }

}
